package components;

public class Elem implements Comparable<Elem>, Sum_I {
	private int n, accumulated;

	public Elem(int n) {
		this.n = n;
		accumulated = n;
	}

	public int getN() {
		return n;
	}

	public int compareTo(Elem other) {
		return n - other.getN();
	}

	public int getAcc() {
		return accumulated;
	}

	public void setAcc(int m) {
	    accumulated = m;
	}

	@Override
	public String toString() {
		return String.format("[value:%d|accumulated:%d]", n, accumulated);
	}
}
