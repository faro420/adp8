package components;

public interface Binaer_I<T extends Comparable<T>> {
	void add(T t);

	void printIn();

	void printPre();

	void printPost();

	int sum(int min, int max);
}
