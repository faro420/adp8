package adp08;

import components.Binaer_I;
import components.Node;
import components.Sum_I;

public class Tree<T extends Comparable<T> & Sum_I> implements Binaer_I<T> {
    private Node<T> head = new Node<T>();
    private int size;
    private T min, max;
    private int sum;
    private boolean f1 = false;
    private boolean  f2 = false;

    public void add(T t) {
        if (size == 0) {
            head.setData(t);
        } else {
            addRekursiv(t, head);
        }
        size++;
    }

    private void addRekursiv(T t, Node<T> parent) {
        int comp = parent.getData().compareTo(t);
        if (comp <= 0) {
            if (parent.getRight() != null) {
                addRekursiv(t, parent.getRight());
            } else {
                Node<T> right = new Node<T>();
                right.setData(t);
                parent.setRight(right);
            }
        } else {
            if (parent.getLeft() != null) {
                addRekursiv(t, parent.getLeft());
            } else {
                Node<T> left = new Node<T>();
                left.setData(t);
                parent.setLeft(left);
            }
        }
    }

    public void printIn() {
        inOrder(head);
    }

    private void inOrder(Node<T> parent) {
        Node<T> puffer = parent.getLeft();
        if (puffer != null) {
            inOrder(puffer);
        }
        System.out.println(parent);
        puffer = parent.getRight();
        if (puffer != null) {
            inOrder(puffer);
        }
    }

    public void printPre() {
        preOrder(head);
    }

    private void preOrder(Node<T> parent) {
        System.out.println(parent);
        Node<T> puffer = parent.getLeft();
        if (puffer != null) {
            preOrder(puffer);
        }
        puffer = parent.getRight();
        if (puffer != null) {
            preOrder(puffer);
        }
    }

    public void printPost() {
        postOrder(head);
    }

    private void postOrder(Node<T> parent) {
        Node<T> puffer = parent.getLeft();
        if (puffer != null) {
            postOrder(puffer);
        }
        puffer = parent.getRight();
        if (puffer != null) {
            postOrder(puffer);
        }
        System.out.println(parent);
    }

    public int size() {
        return size;
    }

    public Node<T> getHead() {
        return head;
    }

    public void init() {
        initRe(head);
    }

    private void initRe(Node<T> parent) {
        Node<T> puffer = parent.getLeft();
        if (puffer != null) {
            initRe(puffer);
        }
        sum += parent.getData().getN();
        parent.getData().setAcc(sum);
        puffer = parent.getRight();
        if (puffer != null) {
            initRe(puffer);
        }
    }

    public int sum(int minimum, int maximum) {
        int comp = head.getData().getN();
        if (comp <= minimum) {
            min = head.getData();
        } else if (comp >= maximum) {
            max = head.getData();
        }
        sumRe(minimum, maximum, head);
        return max.getAcc() - min.getAcc() + min.getN();
    }

    private void sumRe(int minimum, int maximum, Node<T> parent) {
        Node<T> puffer = parent.getLeft();
        if (puffer != null) {
            sumRe(minimum, maximum, puffer);
        }
        int comp = parent.getData().getN();
        if (comp == minimum && f1 == false) {
            this.min = parent.getData();
            f1 = true;
        }
        if (comp == maximum && f2 == false) {
            this.max = parent.getData();
            f2 = true;
        }
        puffer = parent.getRight();
        if (puffer != null) {
            sumRe(minimum, maximum, puffer);
        }
    }
}
