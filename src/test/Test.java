package test;


import adp08.Tree;
import components.Elem;

public class Test {
	public static void main(String[] args) {
		Tree<Elem> tree = new Tree<Elem>();
			tree.add(new Elem(5));
			tree.add(new Elem(6));
			tree.add(new Elem(8));
			tree.add(new Elem(7));
			tree.add(new Elem(2));
			tree.add(new Elem(1));
			tree.add(new Elem(3));
		
        tree.init();
        System.out.println(tree.sum(5,8)); 
		tree.printIn();
	}
}
